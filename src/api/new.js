import request from '@/utils/request'

export const pd = () => {
  return request({
    url: '/v1_0/user/channels'
  })
}
// 新闻
export const xw = (params) => {
  return request({
    url: '/v1_0/articles',
    params
  })
}
//  /v1_0/user/channels
// 获取全部频道
export const qb = (data) => {
  return request({
    url: '/v1_0/channels',
    data
  })
}
// 保存
export const cjh = (data) => {
  return request({
    url: '/v1_0/user/channels',
    data
  })
}
