import request from '@/utils/request'
export const delu = (data) => {
  return request(
    {
      url: '/v1_0/authorizations',
      method: 'post',
      data
    }
  )
}

export function userinfo () {
  return request({
    url: '/v1_0/user/profile'
  })
}

export function photo (data) {
  return request({
    url: '/v1_0/user/photo',
    method: 'patch',
    data
  })
}
export function gr (data) {
  return request({
    url: '/v1_0/user/profile',
    method: 'patch',
    data
  })
}
