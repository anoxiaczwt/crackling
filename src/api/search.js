import request from '@/utils/request'
export const jy = (params) => {
  return request({
    url: '/v1_0/suggestion',
    params
  })
}
// 搜索结果的接口
export const jg = (params) => {
  return request({
    url: '/v1_0/search',
    params
  })
}
