import request from '@/utils/request'
export const pl = (params) => {
  return request({
    url: '/v1_0/comments',
    params
  })
}
// 文章评论 和回复评论
export const wzpl = (data) => {
  return request({
    url: '/v1_0/comments',
    method: 'post',
    data
  })
}
