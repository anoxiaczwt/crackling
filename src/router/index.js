import Vue from 'vue'
import { Toast } from 'vant'
import VueRouter from 'vue-router'
const login = () => import('@/views/login')
const layout = () => import('@/views/layout')
const ask = () => import('@/views/ask')
const home = () => import('@/views/home')
const video = () => import('@/views/video')
const mine = () => import('@/views/mine')
const store = () => import('@/store')
const edit = () => import('@/views/mine/edit')
const article = () => import('@/views/article')
const search = () => import('@/views/search')
const sjg = () => import('@/views/search/sjg')
const chan = () => import('@/views/mine/chan')
Vue.use(VueRouter)
const routes = [
  { name: 'chan', path: '/chan', component: chan },
  { name: 'sjg', path: '/search/sjg', component: sjg },
  { name: 'search', path: '/search', component: search },
  { name: 'article', path: '/article/:id', component: article },
  { name: 'login', path: '/login', component: login },
  { name: 'edit', path: '/mine/edit', component: edit },
  {
    name: 'layout',
    path: '/',
    component: layout,
    redirect: '/home',
    children: [
      { name: 'home', path: '/home', component: home },
      { name: 'ask', path: '/ask', component: ask },
      { name: 'video', path: '/video', component: video },
      {
        name: 'mine',
        path: '/mine',
        component: mine,
        meta: {
          bb: true
        }
      }
    ]
  }

]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  console.log(to, from)
  if (to.meta.bb) {
    if (store.state.user.tokenobj.token) {
      next()
    } else {
      Toast('请先登录')
      next({
        name: 'login',
        query: {
          back: to.path
        }
      })
    }
  } else {
    next()
  }
})

// 解决3.1版本后在控制台出现的警告
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location, onResolve, onReject) {
  if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}
export default router
