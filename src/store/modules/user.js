import { userinfo } from '@/api/user'
export default {
  namespaced: true,
  state () {
    return {
      tokenobj: {},
      // 发请求的总数据
      userinfo: {}
    }
  },
  mutations: {
    req (state, list) {
      state.tokenobj = list
    },
    requser (state, list) {
      state.userinfo = list
    }
  },
  actions: {
    async hhhh (ctx, state) {
      console.log(ctx)
      if (!ctx.state.userinfo.name) {
        const res = await userinfo()
        ctx.commit('requser', res.data)
      }
    }
  }

}
