import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/styles/base.less'
import './utils/vant'
import 'amfe-flexible'
import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
dayjs.extend(relativeTime)

require('dayjs/locale/zh')
// import 'dayjs/locale/de' // ES 2015

dayjs.locale('zh')
console.log(dayjs('2024-9-10 9:00').fromNow())
Vue.config.productionTip = false
Vue.filter('ve', (val) => {
  return dayjs(val).fromNow()
})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
